//author Ab Naffi Ahanger Roll_no(54)
// array_pointer_similarity.c
// This program demonstrates similarites between array and pointer

#include <stdio.h>
#include <assert.h>

int main(void){
  int naffi_a [] = {10, 20, 30, 40, 50, 60, 70, 80};
  int *naffi_p = naffi_a;

  assert(naffi_a == &naffi_a[0]);
  assert(naffi_p == &naffi_a[0]);
  assert(naffi_p[0] == naffi_a[0]);
  assert(naffi_p[2] == 30);
  assert(*(naffi_p + 3) == 40);
  assert(*(naffi_p + 5) == naffi_a[5]);
  assert(*(naffi_a + 3) == 40);
  assert(*(naffi_a + 5) == naffi_p[5]);

  return 0;
}
