//BYTE MASKING
//Autor: nafi

#include<stdio.h>

void print_byte(unsigned char nafi_x ,unsigned char mask)

{
 if((nafi_x & mask)==0)

   printf("0");

 else

   printf("1");
}


int main(void)
{
  unsigned char nafi_x;
  int val;
  int mask;

  printf("enter a byte\n");
  scanf("%x",&val);

  nafi_x=(unsigned char)val;
  
  
    print_byte(nafi_x,0x80);
    print_byte(nafi_x,0x40);
    print_byte(nafi_x,0x20);
    print_byte(nafi_x,0x10);
    print_byte(nafi_x,0x08);
    print_byte(nafi_x,0x04);
    print_byte(nafi_x,0x02);
    print_byte(nafi_x,0x01);  
    printf("\n");
    
    return 0;
}
