//author Ab Naffi Ahanger,Roll_no(54)
// call_by_value_.c
// A program to demonstrate that you pointers
// is a way to simulate a call-by-reference
// in C which only knows call-by-value.
#include <stdio.h>

void try_increment_by_value(int x){     //increment function
  x++;
}

void increment_using_pointers(int *x){  //increment to pointer
  (*x)++;
}

int main(void){
  int naffi_x = 100;                   //variable declaration & initializatio

  printf("x = %d\n", naffi_x);
  try_increment_by_value(naffi_x);
  printf("Now x is after incrementing by value is %d\n", naffi_x);

  increment_using_pointers(&naffi_x);
  printf("After using pointers, we have changed the value to %d\n", naffi_x);

  return 0;
}
