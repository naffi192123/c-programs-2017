// Author Ab naffi Ahanger Roll_no(54)
// two_dimensional_array.c
// A program to show how to populate and print
// a two dimensional array

#include <stdio.h>

#define MAX_SIZE 5                     // preprocessor directive

int main(void){	
  int A[][MAX_SIZE] = {1, 2, 3, 4, 5,		//declaration and initialization of 2D array
		       3, 4, 5, 6, 7,
		       1, 1, 1, 1, 1,
		       0, 0, 0, 0, 0,
		       9, 8, 7, 5, 6};

  for(int i = 0; i < MAX_SIZE; i++){            // printing 2D array with nested for loops
    for(int j = 0; j < MAX_SIZE; j++){
      printf("%d ", A[i][j]);
    }
    printf("\n");
  }

  return 0;
}
