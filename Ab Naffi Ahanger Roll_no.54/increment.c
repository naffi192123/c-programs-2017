// Author Ab Naffi Ahanger Roll_no(54)


#include <stdio.h>

int main(void){
  int naffi_x = 10;
  int naffi_y = naffi_x++;

  printf("naffi_y = %d, naffi_x = %d\n", naffi_y, naffi_x);
  
  int naffi_z = ++naffi_x;

  printf("naffi_z = %d, naffi_x = %d\n", naffi_z, naffi_x);
  return 0;
}

  
