//  author Ab Naffi Ahanger, Roll_no(54)
//  naffi_ menu.c
// A use case of do-while loop

#include <stdio.h>

int show_menu(void){
  int naffi_choice;

  printf("Choose a color ? \n"
	 "1. Red\n"
	 "2. Blue\n"
	 "3. Green\n"
	 "\nChoice (1-3) ? ");
  scanf("%d", &naffi_choice);
  
  if( (naffi_choice >= 1) && (naffi_choice <= 3) ){
    return naffi_choice;
  }else {
    return -1;
  }
}

int main(void){
  int val;
  do {
    val = show_menu();
  }while(val == -1);

  printf("Thanks!\n");
  return 0;
}
