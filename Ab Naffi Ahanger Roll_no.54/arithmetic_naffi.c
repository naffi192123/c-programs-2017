//author Ab Naffi ahnager,Roll_no(54)
// arithmetic_naffi.c
// A program to demonstrate how pointer's arithmetic is done

#include <stdio.h>                                          //header file

#define PRINT_FORMAT "%10s|%20p|%20p|%20p\n"               // preprocessor directive

int main(void){
  char naffi_char_var = 'c';                                     // declaration & initialization of variables
  short naffi_short_var = 10;
  int naffi_int_var = 10;
  double naffi_double_var = 40.00;
  
  char *naffi_char_pointer = &naffi_char_var;                        // declaration & initialization of ponters
  short *naffi_short_pointer = &naffi_short_var;
  int  *naffi_int_pointer = &naffi_int_var;
  double *naffi_double_pointer = &naffi_double_var;

  printf("%10s|%20s|%20s|%20s\n", "Data Type", "Address", "Address-1", "Address+1");
  printf(PRINT_FORMAT, "char", naffi_char_pointer, naffi_char_pointer-1, naffi_char_pointer+1);
  printf(PRINT_FORMAT, "short", naffi_short_pointer, naffi_short_pointer-1, naffi_short_pointer+1);
  printf(PRINT_FORMAT, "int", naffi_int_pointer, naffi_int_pointer-1, naffi_int_pointer+1);
  printf(PRINT_FORMAT, "double", naffi_double_pointer, naffi_double_pointer-1, naffi_double_pointer+1);

  return 0;
}
