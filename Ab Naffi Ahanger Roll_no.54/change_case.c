//CHANGE CASE
//Autor: nafi

#include<stdio.h>

#define LOWER_CASE_BIT 0x20
#define UPPER_CASE_BIT 0xDF

int is_uppercase(unsigned char nafi_c)
{
  return(nafi_c & CASE_BIT)==0);
}

unsigned char to_uppercase(unsigned char nafi_c)
{
  return (nafi_c & UPPER_CASE_BIT);
}
unsigned char to_lowercase(unsigned char nafi_c)
{
  return (nafi_c | LOWER_CASE_BIT);
}


int main(void)
{
  unsigned char nafi_c;
  printf("enter a character\n");
  scanf("%c",&nafi_c);
  if(is_uppercase(nafi_c))
    {
      printf("l_case value is:%c",to_lowercase(nafi_c));
    }
  else
    {
      printf("u_case value is:%c",to_uppercase(nafi_c));
    }
  return 0;
}
