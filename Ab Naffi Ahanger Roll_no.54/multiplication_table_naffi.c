//  author Ab Naffi Ahanger, Roll_no(54)
// naffi_ multiplication_table.c

// Multiplication table for a given number

#include <stdio.h>

int main(void){
  int naffi_num;

  puts("Enter a number ?");
  scanf("%d", &naffi_num);

  int i = 1;
  while(i <= 10){
    int result = naffi_num * i; 
    printf("%d X %d = %d\n", naffi_num, i, result);
    i++;
  }

  return 0;
}
