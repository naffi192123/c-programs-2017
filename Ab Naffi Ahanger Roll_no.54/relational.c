// Author Ab Naffi Ahanger Roll_no(54)
// program to show use of relational operators

#include <stdio.h>
#include <assert.h>

int main(void){
  int naffi_x = 10;
  int naffi_y = 20;
  int naffi_z;
  
  assert(naffi_x < naffi_y);
  assert(naffi_y > naffi_x);
  assert(naffi_x != naffi_y);
  assert(5 < 6);
  assert(5 <= 5);
  assert(10 >= 5);
  assert(10 >= 10);
  assert(naffi_x < naffi_y && naffi_y < 100);
  assert(naffi_x > naffi_y || naffi_y < 100);

  assert(!0);
  assert(1);
  assert(naffi_z = 1);
  assert(!(naffi_z = 0));

  return 0;
}
