//author Ab Naffi Ahanger Roll_no(54)
// pass.c
// Program to check if a student has passed the exam or not.


#include <stdio.h>

#define PASS_PERCENTAGE 40             //preprocessor directive

float get_percentage();

int main(void){
  float naffi_percentage = get_percentage();   //variable declaration and function call

  if(naffi_percentage >= PASS_PERCENTAGE){     
    puts("You passed!");
  }else {
    puts("You failed!");
  }

  return 0;
}

float get_percentage(){
  float percentage;
  
  puts("So, what is your percentage in the exams ? ");
  scanf("%f", &percentage);

  return percentage;
}

