// Author Ab Naffi Ahanger Roll_no(54)
// program to show use of logical operators


#include <stdio.h>

int main(void){
  int naffi_x = 10;
  int naffi_y = 20;

  (naffi_x > naffi_y) && puts("naffi_x > naffi_y");
  (naffi_y > naffi_x) && puts("naffi_y > naffi_x");
  (naffi_x > naffi_y) || puts("naffi_y > naffi_x");
  (naffi_y > naffi_x) || puts("naffi_x > naffi_y");

  return 0;
}


  
