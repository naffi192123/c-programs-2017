//BITWISE AND
//Autor: nafi

#include<stdio.h>

int main(void)
{
   int nafi_a=0x10;   //binary: 00010000
   int nafi_b=0xFC;   //binary: 11111101
   int nafi_result;   //binary: 00010000 = 0x10
   
   nafi_result = nafi_a & nafi_b;
   
   printf("Result after bitwise AND: %x",nafi_result);
   
   return 0;
}
