//author Ab Naffi Ahanger,Roll_no(54)
// inspect_stack_naffi.c
// A program to inspect memory on stack

#include <stdio.h>

int extern_var;                                     //preprocessor directive

void func(void){                                    //function definition
  int x_naffi;
  printf("Inside a function: %d\n", (int)&x_naffi);
}

int main(void){
  int x;
  int y;
  char c;
  long l;

  printf("%d, %d, %d, %d\n", (int)&x, (int)&y, (int)&c, (int)&l);
  func();
  func();
  printf("External variable = %d\n", (int)&extern_var);
  
  return 0;
}
