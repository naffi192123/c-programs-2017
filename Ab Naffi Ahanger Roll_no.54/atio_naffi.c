//author Ab Naffi Ahanger,Roller_no(54)
// atoi.c

// A program to convert a string to integer.

#include <stdio.h>
#include <ctype.h>
#include <assert.h>


int atoi__(char *str){
  int naffi_result = 0;

  while( (*str != EOF) && isdigit(*str) ){
    naffi_result = naffi_result * 10 + (*str - '0');
    str++;
  }

  return naffi_result;
}


int main(void){

  assert(atoi__("888") == 888);
  assert(atoi__("784abc") == 784);
  assert(atoi__("abcd") == 0);
  assert(atoi__("abcd7474") == 0);

  return 0;
}

