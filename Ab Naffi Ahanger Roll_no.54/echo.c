// Author Ab Naffi Ahanger, Roll_no(54)

#include <stdio.h>

void echo_int();           	//function declaration
void echo_float();		//function declaration
void echo_double();		//function declaration
void echo_string();		//function declaration

void main(){
  echo_int();			//FUNCTION CALL
  echo_float();			//FUNCTION CALL
  echo_double();		//FUNCTION CALL
  echo_string();		//FUNCTION CALL
  
}


void echo_int(void){                     //function definition
  int val;

  printf("Enter an integer ? ");
  scanf("%d", &val);
  printf("You have entered %d\n", val);
}

void echo_float(void){			//function definition
  float val;

  printf("Enter a floating point number? ");
  scanf("%f", &val);
  printf("You have entered %f\n", val);
}

void echo_double(void){			//function definition
  double val;

  printf("Enter a double floating point number? ");
  scanf("%lf", &val);
  printf("You have entered %lf\n", val);
}

void echo_string(void){			//function definition
  char str[100];

  fflush(stdin);
  printf("Enter a string? ");
  scanf("\n%99[^\n]", str);
  
  printf("You have entered %s\n", str);
}

