//author Ab Naffi Ahanger,Roll_no(54)
// array_pointer_differences_naffi.c
// A program to demonstrate the differences between an array and a pointer

#include <stdio.h>

int main(void){
 
  char naffi_s [] = {'a', 'b', 'c', 'd'};
 
  char *ps = "abcd";

 
  printf("Size of s is %lu and size of ps is %lu\n", sizeof(naffi_s), sizeof(ps));

 
  return 0;
}
