//Author Ab Naffi Ahanger Roll_no(54)

#include <stdio.h>

int my_strlen (char s[]) {       // function definiton that calculates string length
  int counter = 0;

  while( s[counter] != '\0' ){
    counter = counter + 1;
  }

  return counter;
}

int main(void){
  char s [] = "Hello Naffi";

  printf("The length of %s is %d\n", s, my_strlen(s));
  return 0;
}
  
