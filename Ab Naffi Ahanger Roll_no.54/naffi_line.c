// author Ab Naffi Ahanger, Roll_no(54)
// naffi_line.c
// Draw a line of a given length.


#include <stdio.h>

void draw_length_line(int length);            // function declaration

void main(){
  int naffi_length;

  printf("Enter the length of the line ? ");
  scanf("%d", &naffi_length);

  draw_length_line(naffi_length);          // function call

  
}

void draw_length_line(int length){                // function definition
  
  int i = 0;

  while(i < length){            //looping statement
    putc('*', stdout);
    i = i + 1;
  }

  putc('\n', stdout);
}
