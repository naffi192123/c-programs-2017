//author Ab Naffi Ahanger, Roll_no(54)
// naffi_greatest.c
// A program to find greatest number amount three integers.

#include <stdio.h>

int naffi_greatest(int a, int b, int c);                //function declaration

void main(){
  
  int naffi_a, naffi_b, naffi_c;                        //variable declaration
  
 
  puts("Enter the three numbers separated by spaces ?");
  scanf("%d %d %d", &naffi_a, &naffi_b, &naffi_c);

  int result = naffi_greatest(naffi_a, naffi_b,naffi_c);     // function call

  printf("Greatest number is : %d\n", result);

 
}

int naffi_greatest(int a, int b, int c){              // function call
 
 int result;
  
 if(a > b){            // compare 'a' with 'b'
   if(a > c){          // compare 'a' with 'c'
      result = a;
    }else {
      result = c;
    }
  }else {
   if(b > c){         // compare 'b' with 'c'
      result = b;
    }else {
      result = c;
    }
  }

  return result;
}
