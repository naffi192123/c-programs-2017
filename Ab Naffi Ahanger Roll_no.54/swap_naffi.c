//author Ab Naffi Ahanger,Roll_no(54)
// swap_naffi.c
// A program to demonstrate how we can change values
// in a function although C only supports call-by-value

#include <stdio.h>
#include <assert.h>

void swap(int *pointer_to_x, int *pointer_to_y){  //swaping function
  int temp = *pointer_to_x;
  *pointer_to_x = *pointer_to_y;
  *pointer_to_y = temp;
}

int main(void){
  int naffi_x = 100;
  int naffi_y = 200;

  swap(&naffi_x, &naffi_y);                       //swap function call

  assert(naffi_x == 200);
  assert(naffi_y == 100);
  return 0;
}
