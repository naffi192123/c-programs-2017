//author Ab Naffi Ahanger,Roll_no(54)
// Naffi_grade.c
// Given the percentage of a student,calculate the grade.


#include <stdio.h>

#define GRADE_A 80
#define GRADE_B 60
#define GRADE_C 50
#define GRADE_D 40

char calculate_grade(float percentage);   //function declration

void main(){
  float naffi_percent;
  puts("What is your percentage in exams ? ");
  scanf("%f", &naffi_percent);


  char grade = calculate_grade(naffi_percent);  //function call

  printf("Your Grade is : %c\n", grade);
  
}


char calculate_grade(float percentage){     // function definition
  char result;
  
  if(percentage >= GRADE_A){
    result = 'A';
  }else if(percentage >= GRADE_B){
    result = 'B';
  }else if(percentage >= GRADE_C){
    result = 'C';
  }else if(percentage >= GRADE_D){
    result = 'D';
  }else {
    result = 'F';
  }

  return result;
}
