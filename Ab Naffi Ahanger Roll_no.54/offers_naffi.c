// author Ab Naffi Ahanger, Roll_no(54)
// offers.c

// tell the customers what facilities then get for a given coupon.

#include <stdio.h>

void output_facilities(int coupon);         // function declaration

void main(){
  int naffi_coupon;

  puts("Enter your coupon value (1-4) ?");
  scanf("%d", &naffi_coupon);

  output_facilities(naffi_coupon);

 
}

void output_facilities(int coupon){
  switch(coupon){
  case 4:
    puts("Swift car");
    break;

  case 3:
    puts("Aulto Car");
    break;
  case 2:
    puts("Pulsar Bike");
    break;
  case 1:
    puts("Cycle");
    break;
  default:
    puts("Nothing");
  }
}
