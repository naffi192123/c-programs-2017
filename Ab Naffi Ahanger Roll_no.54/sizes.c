// Author Naffi Roll_no(54)

#include <stdio.h>
#include <limits.h>
#include <float.h>


int main(void){
  int naffi_int_v;
  short naffi_short_v;
  long naffi_long_v;
  
  printf("Sizes of int=%lu, short=%lu, long=%lu\n", sizeof(naffi_int_v), sizeof(naffi_short_v), sizeof(naffi_long_v));

  printf("Range of short signed int: %d - %d\n", SHRT_MIN, SHRT_MAX);
  printf("Range of long unsigned int: 0 - %lu\n", ULONG_MAX);
  return 0;
}
