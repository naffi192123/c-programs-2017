//Author Ab Naffi Ahanger roll_no(54)
// array.c

// A program to demonstrate how to create an array and how to initialize it

#include <stdio.h>

#define MAX_COUNT 10

void initialize_with_range(int numbers [], int count){                    // initialize array with range
  for(int i = 0; i < count; ++i){
    numbers[i] = i;
  }
}

void print_array(int numbers [], int count){                              // print array
  for(int i = 0; i < count; ++i){
    printf("%d ", numbers[i]);
  }
  printf("\n");
}

int main(void){
  int numbers [MAX_COUNT];                                               // declare array
  char hello [] = {'h', 'e', 'l', 'l', 'o', '\0'};                       //initialize array
  char world [] = "World";						//initialize array
  int  int_values [] = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};	//initialize array	
  
  initialize_with_range(numbers, MAX_COUNT);
  print_array(numbers, MAX_COUNT);

  return 0;
}
