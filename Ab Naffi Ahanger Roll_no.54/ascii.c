// Author Ab Naffi Ahnager Roll_No(54)

#include <stdio.h>

int main(void){
  for(int i = 0; i <= 127; ++i){           // for loop 
    printf("%c = %x\n", (char) i, i);
  }

  return 0;
}
